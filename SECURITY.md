# Security Policy

## Supported Versions

Only >v1.2.2 works.

| Version | Supported          |
| ------- | ------------------ |
| 1.3.0+   | ✅ |
| 1.2.3   | :white_check_mark:                |
| 1.2.2   | :white_check_mark: |
| < 1.2.2   | :x:                |

## Reporting a Vulnerability

Open an issue if something works wrong.
